import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:rxdart/rxdart.dart';

void main() {
  runApp(MyApp());
}

class BleDataWidget extends StatelessWidget {
  const BleDataWidget({Key? key, this.result, this.targetMac})
      : super(key: key);
  final ScanResult? result;
  final String? targetMac;

  @override
  Widget build(BuildContext context) {
    if (targetMac == null) {
      return SizedBox();
    }
    late List<int> data;
    late int data2;

    if (result != null) {
      data = parseAdDataToList(result!.advertisementData
          .serviceData["00004452-0000-1000-8000-00805f9b34fb"]!);
      data2 = getBattery(result!.advertisementData
          .serviceData["00004452-0000-1000-8000-00805f9b34fb"]!);
    } else {
      data = List.generate(30, (index) => 0);
      data2 = -1;
    }

    return Column(
      children: [
        Container(
          height: 360,
          width: 300,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2.0),
          ),
          child: GridView.count(
            padding: EdgeInsets.all(2.0),
            crossAxisCount: 5,
            children: data
                .map((e) => Builder(builder: (context) {
                      switch (e) {
                        case 0:
                          return Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2.0),
                                  color: Color(0xffa9a9a9)),
                              margin: EdgeInsets.all(1.0));
                        case 1:
                          return Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2.0),
                                  color: Color(0xffB3E0EF)),
                              margin: EdgeInsets.all(1.0));
                        default:
                          return Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2.0),
                                  color: Color(0xffa9a9a9)),
                              margin: EdgeInsets.all(1.0));
                      }
                    }))
                .toList(),
          ),
        ),
        Container(
          child: Text(
            " ${data2 == -1 ? "- -" : data2} %",
            style: TextStyle(fontSize: 50),
          ),
        ),
      ],
    );
  }
}

List<int> parseAdDataToList(List<int> data) {
  List<int> result = [];
  for (int i = 0; i < 8; i++) {
    for (int j = 6; j >= 0; j = j - 2) {
      int newValue = ((data[i] & 0xff) >> j) & 0x03;
      result.add(newValue);
    }
  }

  return result.sublist(0, 30);
}

getBattery(List<int> data2) {
  int result;
  print(data2[11]);

  return result = data2[11];
}

changeMac(String deviceId, int index) {
  if (deviceId.length < 23) {
    return "WhizPad_RC-FFFFFFFFFFFF";
  }
  int macNumber = int.parse(
      deviceId.substring(deviceId.length - 12, deviceId.length),
      radix: 16);
  int newMacNumber = macNumber + index;
  if (newMacNumber < 000000000000) {
    newMacNumber = 000000000000;
  }
  String newMactrail = newMacNumber.toRadixString(16);
  String newMacId = deviceId.substring(0, deviceId.length - 12);
  newMacId = newMacId + newMactrail.toUpperCase().padLeft(12, "0");
  print(newMacId);
  return newMacId;
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Controller Test',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: MyHomePage(title: 'Controller Test'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String deviceID = " ";
  BehaviorSubject<ScanResult?> _behaviorSubject =
      BehaviorSubject<ScanResult?>();

  Future<void> initNfc() async {
    bool isAvailable = await NfcManager.instance.isAvailable();
    print(isAvailable);
    if (isAvailable) {
      // Start Session
      NfcManager.instance.startSession(
        onDiscovered: (NfcTag tag) async {
          // Do something with an NfcTag instance.
          List<int> data =
              tag.data['ndef']['cachedMessage']['records'][0]['payload'];
          print(String.fromCharCodes(data));
          setState(() {
            deviceID = String.fromCharCodes(data).substring(3);
          });
        },
      );
    }
  }

  FlutterBlue flutterBlue = FlutterBlue.instance;
  void discoverDeviceAd() {
    // Start scanning
    flutterBlue.startScan(
        // timeout: Duration(seconds: 60),
        allowDuplicates: true);
    // .whenComplete(() => dicoverCompelete());
  }

  @override
  void initState() {
    // TODO: implement initState
    initNfc();
    initBleListener();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // Stop Session
    NfcManager.instance.stopSession();
    // Stop scanning
    flutterBlue.stopScan();
    super.dispose();
  }

  initBleListener() {
    FlutterBlue.instance.scanResults.listen((event) {
      event.forEach((result) {
        if ("WhizPad_RC-${result.device.id.toString().replaceAll(":", "")}" ==
            deviceID) {
          _behaviorSubject.sink.add(result);
        }
      });
    });
  }

  dicoverCompelete() async {
    print("1111");
    await Future.delayed(Duration(seconds: 2), () => discoverDeviceAd());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
                height: 80.0,
                child: Center(
                    child: Text(
                  deviceID,
                  style: TextStyle(fontSize: 26),
                ))),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: 50,
                  child: FloatingActionButton(
                      onPressed: () {
                        String newMacId = changeMac(deviceID, -1);
                        deviceID = newMacId;
                        setState(() {});
                        print('press---');
                      },
                      backgroundColor: Colors.cyan,
                      child: const Icon(Icons.arrow_left)),
                ),
                Container(
                  height: 60.0,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.cyan,
                    ),
                    onPressed: () {
                      discoverDeviceAd();
                    },
                    child: StreamBuilder<bool>(
                        stream: FlutterBlue.instance.isScanning,
                        builder: (context, snapshot) {
                          if (snapshot.data != null && snapshot.data == true) {
                            return TextButton(
                              onPressed: () async {
                                flutterBlue.stopScan();
                                await Future.delayed(
                                  Duration(seconds: 2),
                                );
                                _behaviorSubject.add(null);
                              },
                              style: TextButton.styleFrom(
                                primary: Colors.white,
                                backgroundColor: Colors.cyan,
                              ),
                              child: Text(" Stop scan ",
                                  style: TextStyle(fontSize: 26)),
                            );
                          } else {
                            return Text(
                              " Start scan ",
                              style: TextStyle(fontSize: 26),
                            );
                          }
                        }),
                  ),
                ),
                Container(
                  width: 50,
                  child: FloatingActionButton(
                      onPressed: () {
                        String newMacId = changeMac(deviceID, 1);
                        deviceID = newMacId;
                        print('press+++');
                        setState(() {});
                      },
                      backgroundColor: Colors.cyan,
                      child: const Icon(Icons.arrow_right)),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: TextField(
                controller: TextEditingController()
                  ..text = "WhizPad_RC-08F80D61",
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'MAC',
                ),
                onChanged: (value) {
                  print(value.length);
                  if (value.length == 23) {
                    print(value);
                    deviceID = value;
                    setState(() {});
                    print("update: deviceId: $deviceID");
                  }
                },
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  color: Color(0xffE8E8E8),
                  child: StreamBuilder<ScanResult?>(
                    stream: _behaviorSubject.stream,
                    builder: (c, snapshot) => BleDataWidget(
                      result: snapshot.data,
                      targetMac: deviceID,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
